# this python2 script automatically transfers files to the remote server
# and then deletes the files that were transferred
# it is intended to be run as a systemd service
#cat /lib/systemd/system/transfer.service 
#[Unit]
#Description=transfer
#After=multi-user.target
# 
#[Service]
#Type=simple
#ExecStart=sudo /usr/bin/python /home/pi/transfer.py
#Restart=on-abort
# 
#[Install]
#WantedBy=multi-user.target

from time import sleep
import datetime
import time
import socket
import fcntl
import struct
import uuid
import os
import thread
import email, smtplib, ssl
import glob
import paramiko
from hashlib import md5
import argparse
import warnings
import datetime
import time

local_video_path = "/home/pi/Videos/"
local_picture_path = "/home/pi/Pictures/"
remote_video_path = "/var/www/html/webroot/Videos/"
remote_picture_path = "/var/www/html/webroot/Pictures/"
db_remote_path = "/var/www/html/config/schema/all.db"
db_local_path = "/home/pi/all.db"
port = 22
username = "root"
rsa_private_key = r"/root/.ssh/id_rsa"
dir_local = '/home/pi/'
dir_remote = '/home/pi/'
glob_pattern = '*.*'
main_server_ip = "raspberrypis.local"
warnings.filterwarnings("ignore")
last_retrieved = 0

def agent_auth(transport, username):
    try:
        ki = paramiko.RSAKey.from_private_key_file(rsa_private_key)
    except (Exception, e):
        print('Failed loading' % (rsa_private_key, e))
    agent = paramiko.Agent()
    agent_keys = agent.get_keys() + (ki,)
    if len(agent_keys) == 0:
        return
    for key in agent_keys:
        print('Trying ssh-agent key %s' % key.get_fingerprint())
        try:
            transport.auth_publickey(username, key)
            print('... success!')
            return
        except (paramiko.SSHException, e):
            print('... failed!', e)
            
def fileTransfer(local_path, remote_path):
    try:
        print('Establishing SSH connection to:', main_server_ip, port, '...')
        t = paramiko.Transport((main_server_ip, port))
        t.start_client()
        agent_auth(t, username)
        if not t.is_authenticated():
            print('RSA key auth failed! Trying password login...')
            t.connect(username=username, password=password, hostkey=hostkey)
        else:
            sftp = t.open_session()
        sftp = paramiko.SFTPClient.from_transport(t)
        sftp.put(local_path, remote_path)
        t.close()
    except:
        print('something went wrong in fileTransfer()')
        try:
            t.close()
        except:
            pass
    print('file transfer complete!')
    
def check():
    # pictures
    files = os.listdir (local_picture_path)
    for f in files:
        fileTransfer(local_picture_path+f, remote_picture_path+f)
        try:
            os.remove(local_picture_path+f)
        except OSError as e:  
            print ("Error: %s - %s." % (e.filename, e.strerror))
    # videos
    files = os.listdir (local_video_path)
    for f in files:
        fileTransfer(local_video_path+f, remote_video_path+f)
        try:
            os.remove(local_video_path+f)
        except OSError as e:  
            print ("Error: %s - %s." % (e.filename, e.strerror))

def retrieve():
    try:
        print('Establishing SSH connection to:', main_server_ip, port, '...')
        t = paramiko.Transport((main_server_ip, port))
        t.start_client()
        agent_auth(t, username)
        if not t.is_authenticated():
            print('RSA key auth failed! Trying password login...')
            t.connect(username=username, password=password, hostkey=hostkey)
        else:
            sftp = t.open_session()
        sftp = paramiko.SFTPClient.from_transport(t)
        sftp.get(db_remote_path, db_local_path)
        t.close()
    except:
        print('something went wrong in retrieve()')
        try:
            t.close()
        except:
            pass
    print('file transfer complete!')

while(1):
    check()
    retrieve()
    sleep(10)

