# this python3 script captures motion detection events using cv2 
# this script is intended to be run as a systemd service
# /lib/systemd/system/capture.service 

from picamera import PiCamera
from gpiozero import MotionSensor
from time import sleep
import datetime
import time
import socket
import fcntl
import struct
import uuid
import os
import _thread
import email, smtplib, ssl
from hashlib import md5
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sqlite3
from picamera.array import PiRGBArray
import cv2
import numpy
import shlex
import subprocess
from subprocess import CalledProcessError

# global variables
# schedule[0][1] = 0 means monday 01:00am - 01:59am dont record,
# 1 means do record
schedule = [[1]*24 for i in range(7)] 
# this needs to be set manually, later we make it automatic? 
port = 22
username = "pi"
rsa_private_key = r"/home/pi/.ssh/id_rsa"
dir_local = '/home/pi/'
dir_remote = '/home/pi/'
glob_pattern = '*.*'
local_video_path = "/home/pi/Videos/"
local_picture_path = "/home/pi/Pictures/"
remote_video_path = "/home/pi/Videos/"
remote_picture_path = "/home/pi/Pictures/"
main_server_ip = "raspberrypis.local"
pir = MotionSensor(21)
motion_timeout = 10
video_duration = 10
photo_res = (1280,960)
video_res = (1280,720)
video_framerate = 24
receiver_email = "s.lazarus@student.unsw.edu.au"
detect_boolean = 0   # whether to detect
email_boolean = 0   # whether to send emails
thresh_stdev = 10

def sendEmail(filename, date):
    subject = "Sensor triggered"
    body = ""
    sender_email = "comp6733asdf@gmail.com"
    global receiver_email
    password = "comp6733uytgb"
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Bcc"] = receiver_email  # Recommended for mass emails
    message.attach(MIMEText(body, "plain"))
    filename = "/home/pi/Pictures/" + date + ".jpg"
    try:
        with open(filename, "rb") as attachment:
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {filename}",
        )
        message.attach(part)
        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(sender_email, receiver_email, text)
    except:
        pass

# calculates the euclidean distance between RGB values of a photo
def frameDist(frame1, frame2):
    if frame1 is None:
        return 0
    frame1 = numpy.float32(frame1)
    frame2 = numpy.float32(frame2)
    diff = abs(frame1 - frame2)
    dist = numpy.uint8(diff)
    return dist

# this gets called when motion is detected
def capture():
    global detect_boolean
    global email_boolean
    if detect_boolean != 1:
        print("Motion detection disabled by remote server")
        return
    # check schedule to see if we should capture
    day = (datetime.datetime.now() + datetime.timedelta(hours=11)).weekday()
    hour = (datetime.datetime.now() + datetime.timedelta(hours=11)).hour
    print("day: " + str(day))
    print("hour: " + str(hour))
    print("schedule[day][hour]: " + str(schedule[day][hour]))
    if schedule[day][hour] == 0:
        print("schedule prevents recording")
        return
    global camera
    camera.resolution = photo_res
    date = (datetime.datetime.now() + datetime.timedelta(hours=11)).strftime("%Y_%m_%d_%H_%M_%S")
    print("date:" + date)
    picture_file_name = date + ".jpg"
    video_file_name = date + ".h264"
    camera.start_preview()
    picturePath = local_picture_path + picture_file_name
    videoPath = local_video_path + video_file_name
    camera.capture(picturePath)
    print("Captured picture at " + picturePath)
    # send email
    if email_boolean == 1:
        sendEmail(date + ".jpg", date)
    # capture the video
    camera.framerate = video_framerate
    camera.resolution = video_res
    camera.start_recording(videoPath)
    sleep(video_duration)
    camera.stop_recording()
    # encode the video
    try:
        output = subprocess.check_output("MP4Box -add " + videoPath + " " + local_video_path+date+".mp4", shell=True) #need sudo apt install gpac
        print("Captured video at " + local_video_path+date+'.mp4')
        os.remove(videoPath)
    except CalledProcessError as e:
        print('FAIL:\ncmd:{}\noutput:{}'.format(e.cmd, e.output))
    camera.stop_preview()

# this gets called when button pressed
# this will get replaced with pir sensor when we get them
def wait(pir):
    client = None
    camera.resolution = video_res
    camera.framerate = 4
    rawCapture = PiRGBArray(camera, (1280,720))
    time.sleep(.5)
    motionCounter = 0
    avg = None
    frame1 = None
    frame2 = None
    global lastUploaded
    global min_motion_frames
    fgbg = cv2.createBackgroundSubtractorMOG2(history=30)
    emptyFrame = [[1]*1 for i in range(1)] 
    emptyFrame[0][0] = 9001
    min_frames_learned = 10
    #'''
    with PiRGBArray(camera) as stream:
        while True:
            camera.capture(stream, 'bgr', use_video_port=True)
            sleep(.1)
            # stream.array now contains the image data in BGR order
            frame = stream.array
            frame = cv2.GaussianBlur(frame, (5,5), 0)
            fgmask = fgbg.apply(frame)
            print("nmixtures: " + str(fgbg.getNMixtures()))
            print("nhistory: " + str(fgbg.getHistory()))
            if emptyFrame[0][0] == 9001:
                fgmask.fill(0)
                emptyFrame = fgmask
                stream.seek(0)
                stream.truncate()
                continue
            if min_frames_learned > 0:
                stream.seek(0)
                stream.truncate()
                min_frames_learned -= 1
                continue
            dist = frameDist(emptyFrame, fgmask)
            _, stDev = cv2.meanStdDev(dist)
            print("stDev: " + str(stDev[0]))
            print("mean: " + str(_[0]))
            img = fgmask
            cv2.imwrite("/home/pi/bg.jpg", img)
            # reset the stream before the next capture
            stream.seek(0)
            stream.truncate()
            if stDev[0] > thresh_stdev:
                print("Motion detected!")
                img = fgmask
                cv2.imwrite("/home/pi/bg.jpg", img)
                capture()
                return
    '''
    for f in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        frame3 = f.array
        frame3 = cv2.cvtColor(frame3, cv2.COLOR_BGR2GRAY)
        frame3 = cv2.GaussianBlur(frame3, (5,5), 0)
        dist = frameDist(frame1, frame3)
        frame1 = frame2
        frame2 = frame3
        #_, thresh = cv2.threshold(dist, 5, 255, 0)
        _, stDev = cv2.meanStdDev(dist)
        print("stDev: " + str(stDev[0]))
        print("mean: " + str(_[0]))
        rawCapture.truncate(0)
        if stDev[0] > thresh_stdev:
            print("Motion detected!")
            capture()
            return
        '''
        
        
        
# this thread periodically retrieves the user's surveillance schedule
# it puts the result in a local array that gets used by the camera
# to determine whether to record on a detection
# the file db file is retrieved by transfer daemon and stored at
# /home/pi/all.db
def schedule_update():
    while(1):
        sleep(5) # run this every x seconds
        global email_boolean
        global detect_boolean
        global receiver_email
        global thresh_stdev
        global photo_res
        global video_res
        global schedule
        try:
            conn = sqlite3.connect('/home/pi/all.db')
            c = conn.cursor()
            c.execute('select * from settings where id=4')
            r = c.fetchall()
            if r[0][1] == "1":
                detect_boolean = 1
            else:
                detect_boolean = 0
            c.execute('select * from settings where id=6')
            r = c.fetchall()
            receiver_email = r[0][1]
            c.execute('select * from settings where id=7')
            r = c.fetchall()
            if r[0][1] == "1":
                email_boolean = 1
            else:
                email_boolean = 0
            c.execute('select * from settings where id=10') 
            r = c.fetchall()
            thresh_stdev = int(r[0][1])
            c.execute('select * from settings where id=11') #schedule monday
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[0][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=12')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[1][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=13')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[2][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=14')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[3][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=15')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[4][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=16')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[5][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=17')
            r = c.fetchall()
            st = r[0][1]
            j = 0
            for c in st:
                schedule[6][j] = int(c)
                j += 1
            c = conn.cursor()
            c.execute('select * from settings where id=18') #photo
            r = c.fetchall()
            r = int(r[0][1])
            if r == 1:
                photo_res = (1024,768)
            elif r == 2:
                photo_res = (1280,960)
            elif r == 3:
                photo_res = (1600,1200)
            elif r == 4:
                photo_res = (2048,1536)
            elif r == 5:
                photo_res = (2560,1920)
            c = conn.cursor()
            c.execute('select * from settings where id=19') #video
            r = c.fetchall()
            r = int(r[0][1])
            if r == 1:
                video_res = (720,480)
            elif r == 2:
                video_res = (720,576)
            elif r == 3:
                video_res = (1280,720)
            elif r == 4:
                video_res = (1920,1080)
            print("email_boolean: " + str(email_boolean))
            print("detect_boolean: " + str(detect_boolean))
            print("schedule is : ") 
            print(schedule)
            print("receiver email is : " + str(receiver_email)) 
            print("thresh stdev is: " + str(thresh_stdev) ) 
            print("photo_res  is : " + str(photo_res)) 
            print("video_res  is : " + str(video_res)) 
        except:
            print("exception in schedule_update()")

# get our inet ip address
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
ip_addr = s.getsockname()[0]
s.close()
print(ip_addr)

# get mac address
mac_addr = ':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff)for ele in range(0,8*6,8)][::-1])
print(mac_addr)

try:
    _thread.start_new_thread(schedule_update, ())
except:
   print("Error: unable to start thread")
   exit(1)
camera = PiCamera()
camera.vflip = True
while(1):
    wait(pir)

