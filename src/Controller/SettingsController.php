<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Settings Controller
 *
 * @property \App\Model\Table\SettingsTable $Settings
 *
 * @method \App\Model\Entity\Setting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingsController extends AppController
{
    public function index()
    {

    }

    public function cloud(){
        $username = $this->Settings->get(8,['contain'=>[]]);
        $password = $this->Settings->get(9,['contain'=>[]]);
        if ($this->request->is(['patch', 'post', 'put'])){
            $username->attribute = $this->request->getData()['username'];
            $password->attribute = $this->request->getData()['password'];
            if ($this->request->getData()['password'] == null && $this->Settings->save($username)){
                $this->Flash->success(__('New credential details has been saved.'));
            } else if ($this->request->getData()['password'] != null){
                if ($this->Settings->save($username) && $this->Settings->save($password)) {
                    $this->Flash->success(__('New credential details has been saved.'));
                } else {
                    $this->Flash->error(__('New credential details save failed, please try again later'));
                }
            } else {
                $this->Flash->error(__('New credential details save failed, please try again later'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('username'));
        $this->set(compact('password'));
    }

    public function editpath()
    {
        $photopath = $this->Settings->get(1, ['contain' => []]);
        $videopath = $this->Settings->get(2, ['contain' => []]);
        $path = $this->Settings->get(3, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $path->attribute = $this->request->getData()['path'];
            $photopath->attribute = $this->request->getData()['path'] . '/Pictures';
            $videopath->attribute = $this->request->getData()['path'] . '/Videos';
            if ($this->Settings->save($photopath) && $this->Settings->save($videopath)) {
                shell_exec('ln -sfn /var/www/html/webroot/Pictures ' . $this->request->getData()['path']);
                shell_exec('ln -sfn /var/www/html/webroot/Videos ' . $this->request->getData()['path']);
                $this->Flash->success(__('New path has been saved correctly, please wait for moment to make changes effect.'));
            } else {
                $this->Flash->error(__('New path did not saved, please try again later.'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('photopath'));
        $this->set(compact('videopath'));
        $this->set(compact('path'));
    }

    public function camera(){
        $threshold = $this->Settings->get(10,['contain'=>[]]);
        $photores = $this->Settings->get(18,['contain'=>[]]);
        $videores = $this->Settings->get(19,['contain'=>[]]);
        if ($this->request->is(['post'])){
            $threshold->attribute = $this->request->getData()['threshold'];
            $photores->attribute = $this->request->getData()['photoResolution'];
            $videores->attribute = $this->request->getData()['videoResolution'];
            if ($this->Settings->save($photores) && $this->Settings->save($videores) && $this->Settings->save($threshold)){
                $this->Flash->success(__('New camera settings has been saved'));
            } else {
                $this->Flash->error(__('Error happens on saving camera settings, please try again later'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('threshold'));
        $this->set(compact('photores'));
        $this->set(compact('videores'));
    }

    public function hostname()
    {
        $hostname = $this->Settings->get(5, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hostname->attribute = $this->request->getData()['hostname'];
            if ($this->Settings->save($hostname)) {
                $this->Flash->success(__('Hostname has been changed..'));
            } else {
                $this->Flash->error(__('Hostname update failed'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('hostname'));
    }

    public function email()
    {
        $email = $this->Settings->get(6, ['contain' => []]);
        $notification = $this->Settings->get(7, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $change = false;
            if ($email->attribute != $this->request->getData()['email']){
                $change = true;
            }
            $oldemail = $email->attribute;
            $email->attribute = $this->request->getData()['email'];
            $notification->attribute = $this->request->getData()['notification'];
            if ($this->Settings->save($email)) {
                $this->Flash->success(__('Email alert settings has been updated..'));
                if ($change == true) {
                    $newemail = new Email('default');
                    $newemail->transport('default');
                    $newemail->setEmailFormat('html');
                    $newemail->setFrom('consciouschoiceIE@gmail.com', 'Survilliance Pi');
                    $newemail->setSubject('Your Email preference has changed.');
                    $newemail->setTo($email->attribute);
                    $newemail->setBcc($oldemail);
                    $newemail->send('Hi there,<br><br>It\'s from your surveillance Pi, we have noticed that you email address has been changed. If this is not your activity, please check the control console when you back to home.<br>================================================');
                }
            } else {
                $this->Flash->error(__('Email alert settings update failed'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('email'));
        $this->set(compact('notification'));
    }

    public function sendemail()
    {
        $email = $this->Settings->get(6, ['contain' => []]);
        $newemail = new Email('default');
        $newemail->transport('default');
        $newemail->setEmailFormat('html');
        $newemail->setFrom('consciouschoiceIE@gmail.com', 'Survilliance Pi');
        $newemail->setSubject('Test Request.');
        $newemail->setTo($email->attribute);
        $newemail->send('Hi there,<br><br>This is a test email from your surveillance pi.<br><br>================================================');
        $this->Flash->success('Test email has been sent.');
        return $this->redirect(['controller' => 'Settings', 'action' => 'email']);
    }

    public function workingtime()
    {
        $Mon = $this->Settings->get(11, ['contain' => []]);
        $Tue = $this->Settings->get(12, ['contain' => []]);
        $Wed = $this->Settings->get(13, ['contain' => []]);
        $Thu = $this->Settings->get(14, ['contain' => []]);
        $Fri = $this->Settings->get(15, ['contain' => []]);
        $Sat = $this->Settings->get(16, ['contain' => []]);
        $Sun = $this->Settings->get(17, ['contain' => []]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $thisdata = $this->request->getData();
            if ($this->request->getData()['checkAll'] == 0){
                $thisMon='000000000000000000000000';
                $thisTue=$thisMon;
                $thisWed=$thisMon;
                $thisThu=$thisMon;
                $thisFri=$thisMon;
                $thisSat=$thisMon;
                $thisSun=$thisMon;
            }
            $thisMon = $thisdata['mon0'];
            $thisTue = $thisdata['tue0'];
            $thisWed = $thisdata['wed0'];
            $thisThu = $thisdata['thu0'];
            $thisFri = $thisdata['fri0'];
            $thisSat = $thisdata['sat0'];
            $thisSun = $thisdata['sun0'];
            for ($i = 1; $i < 24; $i++) {
                $thisMon = $thisMon . $thisdata['mon' . $i];
                $thisTue = $thisTue . $thisdata['tue' . $i];
                $thisWed = $thisWed . $thisdata['wed' . $i];
                $thisThu = $thisThu . $thisdata['thu' . $i];
                $thisFri = $thisFri . $thisdata['fri' . $i];
                $thisSat = $thisSat . $thisdata['sat' . $i];
                $thisSun = $thisSun . $thisdata['sun' . $i];
            }
            if ($this->request->getData()['checkAll'] == 1){
                $thisMon='111111111111111111111111';
                $thisTue=$thisMon;
                $thisWed=$thisMon;
                $thisThu=$thisMon;
                $thisFri=$thisMon;
                $thisSat=$thisMon;
                $thisSun=$thisMon;
            }
            $Mon->attribute = $thisMon;
            $Tue->attribute = $thisTue;
            $Wed->attribute = $thisWed;
            $Thu->attribute = $thisThu;
            $Fri->attribute = $thisFri;
            $Sat->attribute = $thisSat;
            $Sun->attribute = $thisSun;

            if ($this->Settings->save($Mon) && $this->Settings->save($Tue) && $this->Settings->save($Wed) && $this->Settings->save($Thu)
                && $this->Settings->save($Fri) && $this->Settings->save($Sat) && $this->Settings->save($Sun)) {
                $this->Flash->success(__('New Working schedule has been set.'));
            } else {
                $this->Flash->error(__('Working schedule saves failed, please try again later.'));
            }
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        }
        $this->set('Mon', $Mon);
        $this->set('Tue', $Tue);
        $this->set('Wed', $Wed);
        $this->set('Thu', $Thu);
        $this->set('Fri', $Fri);
        $this->set('Sat', $Sat);
        $this->set('Sun', $Sun);
    }

    public function changeState()
    {
        $workingstate = $this->Settings->get(4, ['contain' => []]);
        if ($workingstate->attribute == "1") {
            $workingstate->attribute = 0;
            if ($this->Settings->save($workingstate)) {
                $this->Flash->warning(__('Passive infrared sensor is now inactive...'));
            }
        } else {
            $workingstate->attribute = 1;
            if ($this->Settings->save($workingstate)) {
                $this->Flash->success(__('Passive infrared sensor is now activated.'));
            }
        }
        return $this->redirect($this->referer());
    }

    public function changeNotification()
    {
        $workingstate = $this->Settings->get(7, ['contain' => []]);
        if ($workingstate->attribute == "1") {
            $workingstate->attribute = 0;
            if ($this->Settings->save($workingstate)) {
                $this->Flash->warning(__('Email notification is now inactive...'));
            }
        } else {
            $workingstate->attribute = 1;
            if ($this->Settings->save($workingstate)) {
                $this->Flash->success(__('Email notification is now activated.'));
            }
        }
        return $this->redirect($this->referer());
    }

    public function shutdown()
    {
        $this->Flash->warning(__('System shutdown now'));
        $this->redirect($this->referer());
        echo shell_exec('sleep 2;sudo /sbin/shutdown -r now');
        return $this->redirect($this->referer());
    }

    public function reboot()
    {
        $this->Flash->warning(__('System is now rebooting'));
        $this->redirect($this->referer());
        echo shell_exec('sleep 2;sudo /sbin/reboot');
        return $this->redirect($this->referer());
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow([]);
    }
}
