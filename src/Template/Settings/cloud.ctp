<?php
$this->assign('title','Cloud Update settings');

echo $this->Html->css("/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.css");
echo $this->Html->script('/vendor/sbadmin2/datatables/jquery.dataTables.min.js');
echo $this->Html->script('/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.js');
echo $this->Html->script('sbadmin2/demo/datatables-demo.js');
?>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a><?= $this->Html->link('Dashboard', ['controller' => 'Pages', 'action' => 'index']) ?></a>
        </li>
        <li class="breadcrumb-item">Settings</li>
        <li class="breadcrumb-item active">Cloud</li>
    </ol>

    <h1 class="h3 mb-2 text-gray-800"><?= __('Cloud Update settings') ?></h1>
    <p class="mb-4"><br>
        You can modify your cloud credential details at here.
        <br><br>
        Please create a separate password (App only) for this service. We are not suggesting using your regular password.
        <br><br>
    </p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Cloud Storage settings</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <p>Note: username and password are case sensitive.</p>
                <?= $this->Form->create() ?>
                <?php echo $this->Form->control('username',['label'=>'Username:','value'=>$username->attribute,'class'=>'form-control','required'=>'true']);?>
                <br><br>
                <?php echo $this->Form->control('password',['label'=>'Password:','value'=>'','class'=>'form-control','type'=>'password'])?>
                <br><br>
                <span><?= $this->Form->button('Submit',['class'=>'btn btn-primary'])?></span>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
