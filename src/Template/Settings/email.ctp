<?php
$this->assign('title','Email Alert Settings');

echo $this->Html->css("/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.css");
echo $this->Html->script('/vendor/sbadmin2/datatables/jquery.dataTables.min.js');
echo $this->Html->script('/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.js');
echo $this->Html->script('sbadmin2/demo/datatables-demo.js');
?>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a><?= $this->Html->link('Dashboard', ['controller' => 'Pages', 'action' => 'index']) ?></a>
        </li>
        <li class="breadcrumb-item">Settings</li>
        <li class="breadcrumb-item active">Email</li>
    </ol>

    <h1 class="h3 mb-2 text-gray-800"><?= __('Email alert settings') ?></h1>
    <p class="mb-4"><br>
        You can modify your received email address at here.
        <br><br>
    </p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Email alert settings</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <p>Note: email address are case sensitive.</p>
                <?= $this->Form->create() ?>
                <?php echo $this->Form->control('email',['label'=>'Email:','value'=>$email->attribute,'class'=>'form-control','required'=>'true']);?>
                <br><br>
                <?php echo $this->Form->checkbox('notification',['id'=>'email1','value'=>$notification->attribute])?>
                <label class="form-check-label" for="email1">Tick to get notifications</label>
                <br><br>
                <row>
                <span><?= $this->Form->button('Submit',['class'=>'btn btn-primary'])?></span>
                <?= $this->Form->end() ?>
                <span class="margin-left:25px"><?= $this->Html->link('Test Email',['controller'=>'Settings','action'=>'sendemail'],['class'=>'btn btn-secondary'])?></span>
                </row>
            </div>
        </div>
    </div>
</div>
