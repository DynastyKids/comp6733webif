<?php
$this->assign('title','Camera Settings');

echo $this->Html->css("/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.css");
echo $this->Html->script('/vendor/sbadmin2/datatables/jquery.dataTables.min.js');
echo $this->Html->script('/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.js');
echo $this->Html->script('sbadmin2/demo/datatables-demo.js');
?>
<div class="container-fluid">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a><?= $this->Html->link('Dashboard', ['controller' => 'Pages', 'action' => 'index']) ?></a>
        </li>
        <li class="breadcrumb-item">Settings</li>
        <li class="breadcrumb-item active">Camera Settings</li>
    </ol>

    <h1 class="h3 mb-2 text-gray-800"><?= __('Camera settings') ?></h1>
    <p class="mb-4"><br>
        You can modify your camera's resolutions at here.
        <br><br>
    </p>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Resolutions settings</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <?= $this->Form->create() ?>
                <label for="photores">Photo Resolutions</label><br>
                <?php echo $this->Form->select(
                    'photoResolution',
                    [   1 => '1 - 1024*768 (0.8MP)',
                        2 => '2 - 1280*960 (1.3MP)',
                        3 => '3 - 1600*1200 (2MP)',
                        4 => '4 - 2048*1536 (3MP)',
                        5 => '5 - 2560*1920 (5MP)'],
                    [
                        'multiple' => false,
                        'value' => [5],
                    ],['class'=>'form-control','label'=>'Photo Resolutions','id'=>'photores']
                );?>
                <br><br><br>
                <label for="videores">Video Resolutions</label><br>
                <?php echo $this->Form->select(
                    'videoResolution',
                    [   1 =>'1 - 720*480 (NTSC)',
                        2 =>'2 - 720*576 (PAL)',
                        3 =>'3 - 1280*720 (HDTV)',
                        4 =>'4 - 1920*1080 (Full HD)'],
                    [
                        'multiple' => false,
                        'value' => [4],
                    ],['class'=>'form-control','label'=>'Video Resolutions','id'=>'videores']
                );?>
                <br><br><br>
                <?php echo $this->Form->control('threshold',['type'=>'number','class'=>'form-control','label'=>'Detection Sensitivity','min'=>'0','max'=>'255','value'=>$threshold->attribute])?>
                <br><br><br>
                <span><?= $this->Form->button('Submit',['class'=>'btn btn-primary'])?></span>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
