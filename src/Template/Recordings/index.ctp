<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Recording[]|\Cake\Collection\CollectionInterface $recordings
 */

use Cake\I18n\Time;

$this->assign('title', 'Recordings');

echo $this->Html->css("/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.css");
echo $this->Html->script('/vendor/sbadmin2/datatables/jquery.dataTables.min.js');
echo $this->Html->script('/vendor/sbadmin2/datatables/dataTables.bootstrap4.min.js');
echo $this->Html->script('sbadmin2/demo/datatables-demo.js');
?>
<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a><?= $this->Html->link('Dashboard', ['controller'=>'Pages','action'=>'home'])?></a></li>
        <?php if($options == null){?>
            <li class="breadcrumb-item active">Recordings</li>
        <?php }  else {?>
            <li class="breadcrumb-item">
            <a><?= $this->Html->link('Recordings', ['controller'=>'Recordings','action'=>'all'])?></a>
            </li>
            <?php if($options == 0){?>
                <li class="breadcrumb-item active">Photos</li>
            <?php } else if($options == 1){?>
                <li class="breadcrumb-item active">Videos</li>
            <?php }}?>
    </ol><br>
    <h1 class="h3 mb-2 text-gray-800"><?= __('Recordings') ?></h1>
    <p class="mb-4"><br>All recordings will be available to view from here.<br><br>
    <?php if($options != null){
        if ($options == 0){?>
        NB: This page only showing photo records.
    <?php } else if ($options == 1){?>
        NB: This page only showing video records.
    <?php }}?>
    </p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Files</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Record Time</th>
<!--                        <th>Sensor Status</th>-->
                        <th>File Format</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Record Time</th>
                        <th>File Format</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php if ($options == null || $options == 0){?>
                    <?php foreach ($photos as $photo):?>
                        <tr>
                            <?php $time = Time::create(substr($photo,0,4),substr($photo,5,2),substr($photo,8,2),
                                substr($photo,11,2), substr($photo,14,2),substr($photo,17,2))?>
                            <td><?php echo $time->i18nFormat('dd-MM-yyyy HH:mm:ss') ?></td>
<!--                            <td>--><?//= h("Inactive");?><!--</td>-->
                            <td><?= h("Image");?></td>
                            <td class="actions">
                                <?= $this->Html->link('View', ['controller'=>'Recordings','action'=>'view',$photo]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $photo], ['confirm' => __('Are you sure to delete this file?', $photo)]) ?>
                            </td>
                        </tr>
                    <?php endforeach;}?>
                    <?php if ($options==null||$options==1){?>
                    <?php foreach ($videos as $video):?>
                        <tr>
                            <?php $time = Time::create(substr($video,0,4),substr($video,5,2),substr($video,8,2),
                                substr($video,11,2), substr($video,14,2),substr($video,17,2))?>
                            <td><?= h($time->i18nFormat('dd-MM-yyyy HH:mm:ss'))?></td>
<!--                            <td>--><?//= h("Triggered");?><!--</td>-->
                            <td><?= h("Video");?></td>
                            <td class="actions">
                                <?= $this->Html->link('View', ['controller'=>'Recordings','action'=>'view',$video]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $video], ['confirm' => __('Are you sure to delete this file?', $video)]) ?>
                            </td>
                        </tr>
                    <?php endforeach;}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
